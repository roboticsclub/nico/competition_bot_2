package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;


/**
 * Created by nicolas on 4/11/18 in competition_bot_2.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot_2
 * Contact: nico@walbots.com, team@walbots.com
 */


public class Autonomous_GrabGlyph extends AutonomousAction
{
    private boolean pickUpGlyph;    //Indicating whether to pick up or release a glyph

    public Autonomous_GrabGlyph(HardwareController hardware, String name, boolean pickUpGlyph)
    {
        super(hardware, name);
        this.pickUpGlyph = pickUpGlyph;
    }

    @Override
    public void setup()
    {
        //Nothing to set up
    }

    @Override
    public ActionState run()
    {
        if(!pickUpGlyph)
        {
            hardware.initialiseTopGrabberServos();
            hardware.initialiseBottomGrabberServos();
        }else
        {
            hardware.powerTopGrabberServoLeft(250);
            hardware.powerTopGrabberServoRight(250);
            hardware.powerBottomGrabberServoLeft(250);
            hardware.powerBottomGrabberServoRight(250);
        }

        return ActionState.FINISHED;
    }

    @Override
    public void shutdown()
    {
        //Nothing to shut down
    }
}
