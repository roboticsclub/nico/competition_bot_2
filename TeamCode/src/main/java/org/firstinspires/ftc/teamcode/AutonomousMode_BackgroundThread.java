package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;
import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousController;
import org.firstinspires.ftc.teamcode.Walbots7175.internal.LoggingController;


/**
 * The class AutonomousMode_BackgroundThread is running the autonomous mode as a background thread.
 * It is managing the creation and execution of all various actions (AutonomousFunctions) that have
 * to be run during the autonomous period.
 *
 * The autonomous mode will be initialized in the AutonomousMode class.
 */
public class AutonomousMode_BackgroundThread implements Runnable
{
    private HardwareController hardware;            //For accessing the robot's hardware
    private boolean isBlueAlliance;                 //Indicating in which alliance we are in
    private boolean isFrontPlace;                   //Indicating on which place we are starting

    private boolean isAutonomousFinished = false;   //Indicating if all actions are completed

    /**
     * Initializing an AutonomousThread with the current AutonomousMode
     *
     * @param currentAutonomousMode The currently running autonomous mode
     */
    AutonomousMode_BackgroundThread(AutonomousMode currentAutonomousMode)
    {
        this.hardware = currentAutonomousMode.hardware;
        this.isBlueAlliance = currentAutonomousMode.isBlueAlliance();
        this.isFrontPlace = currentAutonomousMode.isFrontPlace();
    }

    /**
     * The run() method is part of the Thread life-cycle and gets called after the Thread was
     * started. Everything should already be initialized for running this autonomous mode in the
     * class AutonomousMode!
     *
     * In this case the run() method only runs once and will be directly returned afterwards. All
     * actions as AutonomousFunctions are instantiated and specified so that the
     * AutonomousController runs through them step by step completing all actions we want to do
     * during autonomous.
     */
    @Override
    public final void run()
    {
        //Check if the autonomous mode is done, if yes return
        if (isAutonomousFinished)
        {
            return;
        }


        //Run all actions
        AutonomousAction[] actions = new AutonomousAction[4];

        //Grab glyph
        actions[0] = new Autonomous_GrabGlyph(hardware, "Grab glyph", true);
        actions[0].timeToWaitAfterFinished = 1000;

        //Pick it up with slide
        actions[1] = new Autonomous_MoveLinearSlide(hardware, "Pick up glyph", true);
        actions[1].timeToWaitAfterFinished = 1000;

        //Knock Off Down
        actions[2] = new Autonomous_KnockOffArm(hardware,"Move Knock Off Arm Down", true);
        actions[2].timeToWaitAfterFinished = 1000;

        //Color sensor
        actions[3] = new Autonomous_ColorSensor(hardware, "Sense Color of Balls");
        actions[3].timeToWaitAfterFinished = 1000;

        //Run actions
        runFunctions(actions);

        //Check out color
        boolean isBlueAtTheRightSide = ((Autonomous_ColorSensor)actions[3]).resultIsBlue;


        if (isBlueAtTheRightSide)
        {
            LoggingController.getCurrentInstance().showLog("Color detected", "Detected Blue");
        }else
        {
            LoggingController.getCurrentInstance().showLog("Color detected", "Detected Red");
        }

        //reset actions
        actions = new AutonomousAction[3];

        //Knock off the ball (or turn back)
        float[] drivingPower = new float[5];
        drivingPower[0] = 0.3f;
        drivingPower[1] = -0.3f;
        drivingPower[2] = 0.3f;
        drivingPower[3] = -0.3f;
        actions[isBlueAtTheRightSide == !isBlueAlliance ? 2 : 0] = new Autonomous_Driving(hardware,"Curving", drivingPower);

        //Turn back (or knock off the ball)
        float[] drivingPower2 = new float[4];
        drivingPower2[0] = -0.3f;
        drivingPower2[1] = 0.3f;
        drivingPower2[2] = -0.3f;
        drivingPower2[3] = 0.3f;
        actions[isBlueAtTheRightSide == !isBlueAlliance ? 0 : 2] = new Autonomous_Driving(hardware,"Curving", drivingPower2);

        actions[0].timeToWaitAfterFinished = 300;
        actions[2].timeToWaitAfterFinished = 300;

        //Knock Off Up
        actions[1] = new Autonomous_KnockOffArm(hardware,"Move Knock Off Arm Up", false);
        actions[1].timeToWaitAfterFinished = 1000;


        //Run actions
        runFunctions(actions);

        //Park Robot in Save Zone
        driveIntoSaveZone();



        //Autonomous mode completed :))
        isAutonomousFinished = true;
    }

    /**
     * The driveIntoSaveZone() method manages that the right method gets called so that the robot
     * always drives into the save zone independent to the starting position.
     */
    private void driveIntoSaveZone()
    {
        if (isFrontPlace)
        {
            driveIntoSaveZone_FrontPlace();
        }else
        {
            driveIntoSaveZone_BackPlace();
        }
    }

    /**
     * The driveIntoSaveZone_FrontPlace() method manages that the robot drives into the save zone
     * when the starting position was in the front.
     */
    private void driveIntoSaveZone_FrontPlace()
    {
        //TODO: Develop autonomous for the front places to park in the save zone
        AutonomousAction[] actions = new AutonomousAction[2];

        //Turn around
        float[] power = new float[4];
        power[0] = isBlueAlliance ? -0.5f : 0.5f;
        power[1] = isBlueAlliance ? 0.5f : -0.5f;
        power[2] = isBlueAlliance ? -0.5f : 0.5f;
        power[3] = isBlueAlliance ? 0.5f : -0.5f;
        actions[0] = new Autonomous_Driving(hardware, "Turn around", power);
        actions[0].timeToWaitAfterFinished = 1000;

        //Drive into save zone
        power = new float[4];
        power[0] = -0.5f;
        power[1] = -0.5f;
        power[2] = -0.5f;
        power[3] = -0.5f;
        actions[1] = new Autonomous_Driving(hardware,"Drive Forward", power);
        actions[1].timeToWaitAfterFinished = 1000;
    }

    /**
     * The driveIntoSaveZone_BackPlace() method manages that the robot drives into the save zone
     * when the starting position was in the back.
     */
    private void driveIntoSaveZone_BackPlace()
    {
        AutonomousAction[] actions = new AutonomousAction[3];

        //Drive off balancing board
        float[] power = new float[4];
        power[0] = -0.5f;
        power[1] = -0.5f;
        power[2] = -0.5f;
        power[3] = -0.5f;
        actions[0] = new Autonomous_Driving(hardware,"Drive Forward", power);
        actions[0].timeToWaitAfterFinished = 1000;

        //Turning around to face the save zone
        power = new float[4];
        power[0] = isBlueAlliance ? -0.5f : 0.5f;
        power[1] = isBlueAlliance ? 0.5f : -0.5f;
        power[2] = isBlueAlliance ? -0.5f : 0.5f;
        power[3] = isBlueAlliance ? 0.5f : -0.5f;
        actions[1] = new Autonomous_Driving(hardware,"Curve", power);
        actions[1].timeToWaitAfterFinished = 1000;

        //Drive into save zone
        power = new float[4];
        power[0] = -0.5f;
        power[1] = -0.5f;
        power[2] = -0.5f;
        power[3] = -0.5f;
        actions[2] = new Autonomous_Driving(hardware,"Drive Forward", power);
        actions[2].timeToWaitAfterFinished = 1000;

        //Run actions
        runFunctions(actions);
    }


    /**
     * The runFunctions() method creates an AutonomousController that runs the given
     * AutonomousFunctions and takes care about their execution step by step
     */
    private void runFunctions(AutonomousAction[] functions)
    {
        AutonomousController controller = new AutonomousController(functions);
        try {
            controller.runAllFunctions();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
