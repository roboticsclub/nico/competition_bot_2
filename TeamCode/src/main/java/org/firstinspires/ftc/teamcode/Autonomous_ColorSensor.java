package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;


/**
 * Created by nicolas on 4/11/18 in competition_bot_2.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot_2
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class Autonomous_ColorSensor is an AutonomousAction that handles the color sensor to get
 * information about which color the object has, that is in front of the sensor.
 */
public class Autonomous_ColorSensor extends AutonomousAction
{
    /**
     * The resultIsBlue property indicates if the object in front of the color sensor is blue or red
     */
    public boolean resultIsBlue = false;

    /**
     * Initializes an instance of Autonomous_ColorSensor
     *
     * @param hardware The hardware controller object of the robot to access and control it's hardware
     * @param name The name of the instance of the AutonomousAction (will be displayed on the phone)
     */
    public Autonomous_ColorSensor(HardwareController hardware, String name)
    {
        super(hardware, name);
    }


    /**
     * The setup() method is called by the AutonomousController for setting up the
     * AutonomousAction preparing it for the run() method
     *
     * In this case the LED of the color sensor is turned on
     */
    @Override
    public void setup()
    {
        hardware.enableKnockOffColorSensorLED(true);
    }


    private int count = 0;  //Indicating which iteration of the run() method it is

    /**
     * The run() method is called continually by the AutonomousController until the
     * function returns FINISHED (runs as long as it returns RUNNING)
     *
     * This method will check the color values of the color sensor and store the result in
     * resultIsBlue.
     *
     * @return RUNNING, FINISHED, (nil=error)
     */
    @Override
    public ActionState run()
    {
        count++;
        int blue = hardware.getKnockOffColorSensorBlue();
        int red = hardware.getKnockOffColorSensorRed();

        //resultIsBlue = blue > red;

        //if(blue - red > 10 || red - blue > 10)
        //{
        //    return FunctionState.FINISHED;
        //}

        resultIsBlue = red < 240 ? true : resultIsBlue;

        if (count == 10)
        {
            return ActionState.FINISHED;
        }

        return ActionState.RUNNING;
    }

    /**
     * The shutdown() method is called by the AutonomousController for finishing up all tasks and
     * getting to a state where the AutonomousAction is completed.
     *
     * In this case the LED of the color sensor is turned off
     */
    @Override
    public void shutdown()
    {
        hardware.enableKnockOffColorSensorLED(false);
    }
}
