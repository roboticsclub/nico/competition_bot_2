package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;


/**
 * Created by nicolas on 4/11/18 in competition_bot_2.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot_2
 * Contact: nico@walbots.com, team@walbots.com
 */


public class Autonomous_MoveLinearSlide extends AutonomousAction
{
    private boolean moveUp;

    public Autonomous_MoveLinearSlide(HardwareController hardware, String name, boolean moveUp)
    {
        super(hardware, name);
        this.moveUp = moveUp;
    }

    @Override
    public void setup()
    {

    }

    @Override
    public ActionState run()
    {
        hardware.powerLinearSlideMotors(moveUp ? 0.5f : -0.5f);
        return ActionState.FINISHED;
    }

    @Override
    public void shutdown()
    {
        hardware.powerLinearSlideMotors(0);
    }
}
